//
// Created by monteth on 12/11/17.
//

#ifndef ZMPO_5_VALUES_H
#define ZMPO_5_VALUES_H

#include <string>

#define sPASSED "passed"

#define sFAILED "failed"
#define sDEFAULT_VALUE  "template"
#define sSUM "+"
#define sSUP "^"
#define sSUB "-"
#define sMUL "*"
#define sDIV "/"

#define dDEFAULT_VALUE 1.0;
#endif //ZMPO_5_VALUES_H
