//
// Created by monteth on 12/11/17.
//

#include <iostream>
#include "Node.h"
#include "typeinfo"
#include "Operators.h"
#include "../Values.h"


template<class T>
Node<T>::~Node() {
    children.clear();
}

template<class T>
Node<T>::Node() {
    type = NONE;
    children = std::vector<Node<T>>();
}

template<class T>
T Node<T>::comp() {
    T result;
    switch (type) {
        case SUM:
            result = children[0].comp() + children[1].comp();
            break;
        case SUB:
            result = children[0].comp() - children[1].comp();
            break;
        case MUL:
            result = children[0].comp() * children[1].comp();
            break;
        case DIV:
            result = children[0].comp() / children[1].comp();
            break;
        case VAL:
            result = value;
            break;
        case NONE:
            break;
        case SUP:
            result = superAdd(children[0].comp(), children[1].comp(), children[2].comp());
            break;
    }
    return result;
}

template<class T>
std::string Node<T>::getPrefix() {
    std::string result = sValue;
    switch (type) {
        case SUM:
            result = sSUM;
            break;
        case SUB:
            result = sSUB;
            break;
        case DIV:
            result = sDIV;
            break;
        case MUL:
            result = sMUL;
            break;
        case VAL:
            result = getStringValue();
            break;
        case NONE:
            break;
        case SUP:
            result = sSUP;
            break;
    }
    for (int i = 0; i < children.size(); ++i) {
        result += " " + children[i].getPrefix();
    }
    return result;
}

template<class T>
std::string Node<T>::getStringValue() {
    return std::to_string(value);
}

template<>
std::string Node<std::string>::getStringValue() {
    return value;
}

template<class T>
T Node<T>::getDefValue() {
    return (T) dDEFAULT_VALUE;
}

template<>
std::string Node<std::string>::getDefValue() {
    return sDEFAULT_VALUE;
}

template<class T>
Node<T>::Node(const std::string *inputArray, int &actualIndex, int maxIndex, bool &everythingFine) {
    if (actualIndex >= maxIndex) {
        type = VAL;
        value = getDefValue();
        actualIndex++;
    } else {
        while (!(isProperString(inputArray[actualIndex]) || Tools::isOperator(inputArray[actualIndex])) &&
               (actualIndex < maxIndex)) {
            actualIndex++;
            everythingFine = false;
        }
        if (actualIndex >= maxIndex) {
            type = VAL;
            value = getDefValue();
            actualIndex++;
        } else {
            std::string thisString = inputArray[actualIndex];
            if (Tools::isOperator(thisString)) {
                type = Tools::getOperator(thisString);
            } else {
                type = VAL;
                value = getTVal(thisString);
            }
            actualIndex++;
            children = std::vector<Node<T>>();
            for (int i = 0; i < Tools::getQuantOfChildren(type); ++i) {
                children.emplace_back(Node(inputArray, actualIndex, maxIndex, everythingFine));
            }
        }
    }
}

template<class T>
T Node<T>::getTVal(std::string inputString) {
    return (T) std::stod(inputString);
}

template<>
bool Node<int>::isProperString(const std::string inputString) {
    return Tools::isInt(inputString);
}

template<class T>
T Node<T>::superAdd(T element1, T element2, T element3) {
    return element1 + element2 + element3;
}

template<>
bool Node<double>::isProperString(const std::string inputString) {
    return Tools::isDouble(inputString);
}

template<>
bool Node<std::string>::isProperString(const std::string inputString) {
    return true;
}

template<>
std::string Node<std::string>::getTVal(std::string inputString) {
    return inputString;
}

template
class Node<int>;

template
class Node<double>;

template
class Node<std::string>;