//
// Created by monteth on 12/17/17.
//

#ifndef ZMPO_5_STRINGOPERATORS_H
#define ZMPO_5_STRINGOPERATORS_H


#include <string>

std::string operator + (std::string str1, std::string str2);
std::string operator - (std::string str1, std::string str2);
std::string operator / (std::string str1, std::string str2);
std::string operator * (std::string str1, std::string str2);

#endif //ZMPO_5_STRINGOPERATORS_H
