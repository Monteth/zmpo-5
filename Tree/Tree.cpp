//
// Created by monteth on 12/11/17.
//

#include "Tree.h"
template<class T>
Tree<T>::Tree() {
    root = Node<T>();
}

template <class T>
bool Tree<T>::enter(std::string inputString) {
    int commandsLength = 0;
    std::string *commandsArray;
    commandsArray = Tools::split(inputString, ' ', &commandsLength);
    //commandsLength--;
    int actualIndex = 0;
    bool everythingFine = true;
    root = Node<T>(commandsArray, actualIndex, commandsLength, everythingFine);
    if (actualIndex > commandsLength) everythingFine = false;
    return everythingFine;
}

template<class T>
std::string Tree<T>::getPrefix() {
    return root.getPrefix();
}

template<class T>
T Tree<T>::getResult() {
    return root.comp();
}


template class Tree<int>;
template class Tree<double>;
template class Tree<std::string>;