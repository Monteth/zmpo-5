//
// Created by monteth on 12/11/17.
//

#ifndef ZMPO_5_NODE_H
#define ZMPO_5_NODE_H


#include <string>
#include <vector>
#include "Tools.h"

template<class T>
class Node {
public:
    Node();

    Node(const std::string *inputArray, int &actualIndex, int maxIndex, bool &everythingFine);

    ~Node();

    std::string getPrefix();

    T comp();

private:
    T value;
    std::string sValue;
    std::vector<Node<T>> children;
    NodeType type;

    std::string getStringValue();

    T getDefValue();

    T getTVal(std::string inputString);

    bool isProperString(std::string inputString);

    T superAdd(T element1, T element2, T element3);
};


#endif //ZMPO_5_NODE_H
