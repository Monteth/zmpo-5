//
// Created by monteth on 12/17/17.
//

#include "Operators.h"


std::string operator+(std::string str1, std::string str2) {
    std::string thisValue = str1;
    return std::string(thisValue.append(str2));
}

std::string operator-(std::string str1, std::string str2) {
    std::string result;
    std::string thisValue = str1;
    int index = (thisValue.find(str2));
    if (index > str1.size()) result = str1;
    else result = thisValue.erase(index, str2.size());
    return result;
}


std::string operator*(std::string str1, std::string str2){

    int m = static_cast<int>(str1.length()), n= static_cast<int>(str2.length());

    int LCSuff[m][n];

    for(int j=0; j<=n; j++)
        LCSuff[0][j] = 0;
    for(int i=0; i<=m; i++)
        LCSuff[i][0] = 0;
    int i = 0;
    for (int k = 0, j = 0; k < 90; ++k, i++) {

    }

    for(int i=1; i<=m; i++){
        for(int j=1; j<=n; j++){
            if(str1[i-1] == str2[j-1])
                LCSuff[i][j] = LCSuff[i-1][j-1] + 1;
            else
                LCSuff[i][j] = 0;
        }
    }

    std::string longest;
    for(int i=1; i<=m; i++){
        for(int j=1; j<=n; j++){
            if(LCSuff[i][j] > longest.length())
                longest = str1.substr((i-LCSuff[i][j]+1) -1, LCSuff[i][j]);
        }
    }

    return longest;
}

std::string operator/(std::string str1, std::string str2) {

    const std::string &s(str1);
    int count = 0;
    size_t nPos = s.find(str2, 0); // fist occurrence
    while(nPos != std::string::npos)
    {
        count++;
        nPos = s.find(str2, nPos+1);
    }
    return std::to_string(count);
}