//
// Created by monteth on 12/17/17.
//

#include "Tools.h"

bool Tools::isInt(std::string inputString) {
    if ((inputString[0] != '-') && (inputString[0] < '0' || inputString[0] > '9')) return false;
    if (inputString[0] == '-' && inputString.length() == 1) return false;
    for (int i = 1; i < inputString.length(); i++) {
        if (inputString[i] < '0' || inputString[i] > '9') return false;
    }
    return true;
}

bool Tools::isOperator(std::string inputString) {
    bool result = false;
    if (inputString == "+" ||
        inputString == "-" ||
        inputString == "*" ||
        inputString == "/" ||
        inputString == "^" ) {
        result = true;
    }
    return result;
}

int Tools::getQuantOfChildren(NodeType anOperator) {
    int result;
    if (anOperator == VAL) result = 0;
    else if (anOperator == SUP) result = 3;
    else if (anOperator == NONE) result = 0;
    else result = 2;

    return result;
}

NodeType Tools::getOperator(std::string inputString) {
    NodeType result = VAL;
    if (inputString == "+") result = SUM;
    else if (inputString == "-") result = SUB;
    else if (inputString == "*") result = MUL;
    else if (inputString == "/") result = DIV;
    else if (inputString == "^") result = SUP;
    return result;
}

std::string *Tools::split(std::string str, char separator, int *length) {
    std::string *arrayOfWords;
    arrayOfWords = new std::string[1];
    std::string word;

    for (int i = 0; i <= str.length(); i++) {
        if ((i == str.length() || str[i] == separator) && word.length() > 0) {
            push(arrayOfWords, *length, word);
            (*length)++;
            word = "";
        } else if (str[i] != separator) {
            word += str[i];
        }
    }
    return arrayOfWords;
}

void Tools::push(std::string *&array, int length, std::string newWord) {
    std::string *newArray;
    newArray = new std::string[length + 1];

    for (int i = 0; i < length; i++) {
        newArray[i] = array[i];
    }

    newArray[length] = move(newWord);
    delete[] array;
    array = newArray;
}

bool Tools::isDouble(std::string inputString) {
    try {
        std::stod(inputString);
    } catch (...) {
        return false;
    }
    return true;

}
