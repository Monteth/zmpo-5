//
// Created by monteth on 12/11/17.
//

#ifndef ZMPO_5_TREE_H
#define ZMPO_5_TREE_H

#include "Node.h"

template <class T>
class Tree {
public:
    Tree();
    bool enter(std::string inputString);
    T getResult();
    std::string getPrefix();

private:
    Node<T> root;
};




#endif //ZMPO_5_TREE_H
