//
// Created by monteth on 12/17/17.
//

#ifndef ZMPO_5_TOOLS_H
#define ZMPO_5_TOOLS_H

#include <string>
#include "NodeType.h"

class Tools {
public:
    static bool isOperator(std::string inputString);

    static bool isInt(std::string inputString);

    static bool isDouble(std::string inputString);

    static int getQuantOfChildren(NodeType anOperator);

    static NodeType getOperator(std::string inputString);

    static std::string *split(std::string str, char separator, int *length);

private:
    static void push(std::string *&array, int length, std::string newWord);
};


#endif //ZMPO_5_TOOLS_H
