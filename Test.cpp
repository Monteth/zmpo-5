//
// Created by monteth on 12/18/17.
//

#include <iostream>
#include <utility>
#include "Test.h"
#include "Tree/Tree.h"
#include "Values.h"

void Test::singleTestString(std::string testName, std::string treeInput, std::string expectedResult){
    Tree<std::string> tree;
    std::cout  << testName;
    tree.enter(std::move(treeInput));
    std::cout  << tree.getPrefix() + " ";
    tree.getResult() == expectedResult ? std::cout  << sPASSED << std::endl : std::cout << sFAILED << std::endl;
}

void Test::singleTestInt(std::string testName, std::string treeInput, int expectedResult){
    Tree<int> tree;
    std::cout  << testName;
    tree.enter(std::move(treeInput));
    std::cout  << tree.getPrefix() + " ";
    tree.getResult() == expectedResult ? std::cout  << sPASSED << std::endl : std::cout << sFAILED << std::endl;
}

void Test::singleTestDouble(std::string testName, std::string treeInput, double expectedResult){
    Tree<double> tree;
    std::cout <<testName;
    tree.enter(std::move(treeInput));
    std::cout  << tree.getPrefix() + " ";
    tree.getResult() == expectedResult ? std::cout  << sPASSED << std::endl : std::cout << sFAILED << std::endl;
}

void Test::testStringTree() {
    std::cout  << "Starting string tree tests:" << std::endl;
    singleTestString("Add: ", "+ Ala la", "Alala");
    singleTestString("Sub: ", "- Ala la", "A");
    singleTestString("Mul: ", "* Ala la", "la");
    singleTestString("Div: ", "/ Ala la", "1");
}

void Test::testIntTree() {
    std::cout  << "Starting int tree tests:" << std::endl;
    singleTestInt("Add: ", "+ 4 6", 10);
    singleTestInt("Sub: ", "- 4 6", -2);
    singleTestInt("Mul: ", "* 4 6", 24);
    singleTestInt("Div: ", "/ 4 6", 0);
}
void Test::testDoubleTree() {
    std::cout  << "Starting double tree tests:" << std::endl;
    singleTestDouble("Add: ", "+ 4.5 6.6", 11.1);
    singleTestDouble("Sub: ", "- 4.5 6.5", -2);
    singleTestDouble("Mul: ", "* 4.5 6.6", 29.7);
    singleTestDouble("Div: ", "/ 29.7 6.6", 4.5);
}

void Test::start() {
    testDoubleTree();
    testIntTree();
    testStringTree();
    complexTest();
}

void Test::complexTest() {
    std::cout  << "Starting complex tree tests:" << std::endl;
    singleTestInt("+ + 1 2 3 ", "+ + 1 2 3", 6);
    singleTestInt("+ + 1 2 ", "+ + 1 2", 4);
    singleTestDouble("+ 1.1 * 2.5 2 ", "+ 1.1 * 2.5 2", 6.1);
    singleTestString("+ * ala ma - kota ot ", "+ * ala ma - kota ot", "aka");
    singleTestString("-     aaa b ", "-     aaa b", "aaa");
    singleTestString("/ konstantynopolitańczykowianeczka a ", "/ konstantynopolitańczykowianeczka a", "4");
    singleTestInt("", "^ 3 5 8", 16);
    singleTestDouble("","^ 2.5 3.6 4.7",10.8);
    singleTestString("", "^ ala ma kota", "alamakota");
}
