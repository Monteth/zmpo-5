//
// Created by monteth on 12/18/17.
//

#ifndef ZMPO_5_TEST_H
#define ZMPO_5_TEST_H


#include <string>

class Test {
public:
    static void start();
private:
    static void testStringTree();
    static void testIntTree();
    static void testDoubleTree();
    static void complexTest();
    static void singleTestString(std::string testName, std::string treeInput, std::string expectedResult);
    static void singleTestInt(std::string testName, std::string treeInput, int expectedResult);
    static void singleTestDouble(std::string testName, std::string treeInput, double expectedResult);
};


#endif //ZMPO_5_TEST_H
